import alt from 'alt';
import chat from 'chat';
import * as extended from 'server-extended';
import * as color from './colors.mjs';
console.log(`${color.FgGreen}=====> Roleplay Chat Started `);

alt.on('playerConnect',(player)=>{
chat.broadcast(`{24FF00}${player.name} has connected`);
extended.SetupExportsForPlayer(player);
});

alt.on('playerDisconnect',(player)=>{
    chat.broadcast(` {FF0000} ${player.name} has disconnected`);
});

