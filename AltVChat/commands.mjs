import * as alt from 'alt';
import chat from 'chat';
import * as extended from 'server-extended';

chat.registerCmd('me',(player,args) => {
    if(args.length <= 0){
        chat.send(player, `{FF0000} /me 'your me'` );
        return;
    }
    var fraseme='';
    args.forEach(word => {
        fraseme += ' ' + word
    });
    extended.DisplayAboveHead(player,`${fraseme}`,6000, 10, 190, 165, 200, 255);
    chat.broadcast(`{FF0000} ${player.name} | Me: ${fraseme}`);

});

chat.registerCmd('do',(player,args) => {
    if(args.length <= 0){
        chat.send(player, `{FF0000} /do 'your do'` );
        return;
    }
    var frasedo='';
    args.forEach(word => {
        frasedo += ' ' + word
    });
    extended.DisplayAboveHead(player,`${frasedo}`,6000, 10, 47, 252, 255, 255);
    chat.broadcast(`{00FF0E} ${player.name} | Do: ${frasedo}`);
});

chat.registerCmd('ooc',(player,args) => {
if(args.length <= 0){
    chat.send(player,`{FF0000}/ooc 'your ooc'`);
    return;
}
var fraseooc = '';
args.forEach(word =>{
    fraseooc += ' '+word
});
chat.broadcast(`{6D6D6D}${player.name} | OOC: ${fraseooc}`);
});

chat.registerCmd('twt',(player,args)=>{
    if(args.length <=0){
        chat.send(player, `{FF0000}/twt 'your twt'`);
        return;
    }
    var frasetwt = '';
    args.forEach(word =>{
        frasetwt += ' '+word
    });
    chat.broadcast(`{00F1FF}${player.name} | Tweet: ${frasetwt}`);
});

chat.registerCmd('atwt',(player,args) => {
    if(args.length <=0){
        chat.send(player, `{FF0000}/atwt 'your anon twt'`);
        return;
    }
    var fraseatwt = '';
    args.forEach(word=>{
        fraseatwt += ' '+word
    });
    chat.broadcast(`{00F1FF}Anon TWT: ${fraseatwt}`);
    console.log(`${player.name} Send Atwt: ${fraseatwt}`);
});

